
#personal #myjobnow
# MyJobNow Demo App
## Code language
Το app υλοποιήθηκε σε Kotlin. Επίσης έγινε χρήση  kotlin - coroutines. Για τις κλήσεις χρησιμοποίησα Retrofit και Gson.

## Design Patterns
MVP
Για την υλοποίηση του app χρησιμοποίησα το MVP pattern, με την διαφορά πως πρόσθεσα και ένα ακόμα layer, τον Interactor. Ο Interactor
είναι υπεύθυνος για να “τραβήξει” τα δεδομένα μας (από τη βάση δεδομένων, το web service κλπ) και να τα προωθήσει στον αντίστοιχο presenter που είναι αποκλειστικά υπεύθυνος για τις αλλαγές στο UI. Με τον τρόπο αυτό ο presenter μας γίνεται μικρότερος , πιο εύκολα διαχειρίσιμος και πιο εύκολο για εμάς να γράψουμε τα αντίστοιχα τεστ.

## Third Party Libraries
Όπως φαίνεται και στο Gradle χρησιμοποίησα τις εξής βιβλιοθήκες:
Ιmages
com.squareup.picasso:picasso:2.71828 
Logging
com.jakewharton.timber:timber
Loader
com.github.ybq:Android-SpinKit:1.4.0
Material Dialog
com.afollestad.material-dialogs:core:3.3.0

## UI
Όσον αφορά το UI είναι απλό. Δημιούργησα κάποια styles για τα κουμπιά και την search bar. Σχετικά με τις εικόνες του κάθε αποτελέσματος επέλεξα να κρατήσω από τα sizes το thumbnail αν υπάρχει. Σε περίπτωση που δεν βρεθεί δείχνω το launcher. 

## Error Handling
Γίνεται έλεγχος για generic errors. Επίσης υπάρχουν τα αντίστοιχα μηνύματα στην περίπτωση empty data από το api.

## Unit Test
Επέλεξα να γράψω τεστ για τον presenter και τον interactor, base και search term. 