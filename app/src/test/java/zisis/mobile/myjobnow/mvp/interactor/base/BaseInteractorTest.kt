package zisis.mobile.myjobnow.mvp.interactor.base

import com.nhaarman.mockitokotlin2.verify
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import zisis.example.weatherdemo.mvp.interactor.base.BaseInteractor

class BaseInteractorTest {

    @Mock
    lateinit var baseInteractor: BaseInteractor

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
    }

    @Test
    fun detach() {
        baseInteractor.detach()
        verify(baseInteractor).detach()
    }



}