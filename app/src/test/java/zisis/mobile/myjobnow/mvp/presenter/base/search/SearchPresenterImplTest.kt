package zisis.mobile.myjobnow.mvp.presenter.base.search

import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.nhaarman.mockitokotlin2.atLeastOnce
import com.nhaarman.mockitokotlin2.verify
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import kotlinx.coroutines.test.TestCoroutineScope
import kotlinx.coroutines.test.runBlockingTest
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.MockitoAnnotations
import zisis.mobile.myjobnow.models.DataResult
import zisis.mobile.myjobnow.mvp.interactor.search.SearchInteractorImplTest
import zisis.mobile.myjobnow.mvp.interactor.search.SearchTermInteractor
import zisis.mobile.myjobnow.mvp.presenter.search.SearchTermPresenterImpl
import zisis.mobile.myjobnow.mvp.view.search.SearchTermView
import zisis.mobile.myjobnow.network.parsers.search.SearchTermModel
import java.lang.reflect.Type

class SearchPresenterImplTest {

    private val gson = Gson()
    var collectionType: Type =
        object : TypeToken<Collection<SearchTermModel?>?>() {}.getType()
    var searchTermResponse: Collection<SearchTermModel> =
        gson.fromJson(SearchInteractorImplTest::class.java.getResource("/search_java_term_response.js")!!.readText(), collectionType)

    private val query = "java"
    private val page = 1

    @ExperimentalCoroutinesApi
    private val testUiDispatcher = TestCoroutineDispatcher()
    @ExperimentalCoroutinesApi
    private val testBgDispatcher = TestCoroutineDispatcher()
    @ExperimentalCoroutinesApi
    private val testScope = TestCoroutineScope()

    @Mock
    lateinit var view: SearchTermView
    @Mock
    lateinit var interactor: SearchTermInteractor
    @Mock
    lateinit var presenter: SearchTermPresenterImpl

    @ExperimentalCoroutinesApi
    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        presenter = SearchTermPresenterImpl(view, interactor)
        presenter.updateDispatchersForTests(testUiDispatcher, testBgDispatcher, testScope)
    }

    @OptIn(ExperimentalCoroutinesApi::class)
    @Test
    fun testInit() = testScope.run {
        whenever(view.isAttached()).thenReturn(true)
        presenter.getSuggestions(query)
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getSuggestion() = runBlockingTest {
        whenever(view.isAttached()).thenReturn(true)
        whenever(interactor.getSuggestions(query, page)).thenReturn(
            DataResult(searchTermResponse.toList())
        )

        presenter.getSuggestions(query)
        verify(view, atLeastOnce()).isAttached()
        verify(view).showSuggestions(ArrayList(searchTermResponse), false)
    }
}