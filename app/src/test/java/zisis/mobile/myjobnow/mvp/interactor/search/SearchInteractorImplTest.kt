package zisis.mobile.myjobnow.mvp.interactor.search

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.nhaarman.mockitokotlin2.whenever
import kotlinx.coroutines.CompletableDeferred
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import zisis.mobile.myjobnow.common.application.MyJobNowApplication
import zisis.mobile.myjobnow.common.utils.preferences.AppSharedPreferences
import zisis.mobile.myjobnow.network.parsers.search.SearchTermModel
import zisis.mobile.myjobnow.providers.NetworkProvider
import java.lang.reflect.Type


class SearchInteractorImplTest {

    private val gson = Gson()
    var collectionType: Type =
        object : TypeToken<Collection<SearchTermModel?>?>() {}.getType()
    var searchTermResponse: Collection<SearchTermModel> =
        gson.fromJson(SearchInteractorImplTest::class.java.getResource("/search_java_term_response.js")!!.readText(), collectionType)

    private val totalResults = 10
    private val firstResultId = 1701L
    private val firesResultTitlle = "Android detekt"
    private val query = "java"
    private val page = 1

    @Mock
    lateinit var searchTermInteractor: SearchTermInteractor
    @Mock
    lateinit var networkProvider: NetworkProvider
    @Mock
    lateinit var sharedPreferences: AppSharedPreferences
    @Mock
    private lateinit var context: Context
    @Mock
    private lateinit var application: MyJobNowApplication

    private lateinit var searchTermInteractorImpl: SearchTermInteractorImpl

    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        Mockito.`when`(
            application.appSharedPreferences
            )
        .thenReturn(sharedPreferences)
        searchTermInteractorImpl = SearchTermInteractorImpl(networkProvider, sharedPreferences)
    }

    @After
    fun tearDown() {
        searchTermInteractorImpl.detach()
    }

    @ExperimentalCoroutinesApi
    @Test
    fun getLocationWeatherForecast() = runBlocking {
        whenever(networkProvider.getSearchTermResultAsync(query, page)).thenReturn(
            CompletableDeferred(
                searchTermResponse.toList()
            )
        )
        val response = searchTermInteractorImpl.getSuggestions(query, page)
        Assert.assertNotNull(response)
        Assert.assertNotNull(response.data)
        Assert.assertEquals(totalResults, response.data?.size)
        Assert.assertEquals(firstResultId, response.data?.get(0)?.id)
        Assert.assertEquals(firesResultTitlle, response.data?.get(0)?.getTitle())
    }


}