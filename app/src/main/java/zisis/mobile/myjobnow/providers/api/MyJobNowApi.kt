package zisis.mobile.myjobnow.providers.api

import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import zisis.mobile.myjobnow.network.parsers.search.SearchTermModel
import zisis.mobile.myjobnow.network.parsers.search.SearchTermResponse
import zisis.mobile.myjobnow.network.parsers.search.image.ImageResponse

interface MyJobNowApi {

    @GET("posts")
    fun getSearchTermResultsAsync(@Query("search") searchTerm: String?, @Query("page") page: Int): Deferred<List<SearchTermModel>>

    @GET("media/{id}")
    fun getImageUrlAsync(@Path("id") featuredMediaId : String) : Deferred<ImageResponse>




}