package zisis.mobile.myjobnow.providers

import kotlinx.coroutines.Deferred
import zisis.mobile.myjobnow.network.parsers.search.SearchTermModel
import zisis.mobile.myjobnow.network.parsers.search.SearchTermResponse
import zisis.mobile.myjobnow.network.parsers.search.image.ImageResponse

interface NetworkProvider {

    fun getSearchTermResultAsync(queryText: String?, page: Int): Deferred<List<SearchTermModel>>

    fun getImageUrlAsync(featuredMediaId : String) : Deferred<ImageResponse>
}