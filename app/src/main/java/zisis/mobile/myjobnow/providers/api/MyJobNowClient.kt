package zisis.mobile.myjobnow.providers.api

import android.content.Context
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import zisis.mobile.myjobnow.common.DefinitionsApi
import zisis.mobile.myjobnow.network.parsers.search.SearchTermModel
import zisis.mobile.myjobnow.network.parsers.search.SearchTermResponse
import zisis.mobile.myjobnow.network.parsers.search.image.ImageResponse
import zisis.mobile.myjobnow.providers.NetworkProvider
import java.lang.ref.WeakReference
import java.util.concurrent.TimeUnit

class MyJobNowClient(private val appContext: Context): NetworkProvider {

    private var myJobNowApi : MyJobNowApi
    private var applicationContext: WeakReference<Context>? = null

    init {
        applicationContext = WeakReference(appContext)
        myJobNowApi = createApi(appContext)
    }

    private fun createApi(appContext: Context): MyJobNowApi {
        return retrofit().create(MyJobNowApi::class.java)
    }

    private fun getOkHttpClient(): OkHttpClient {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val builder = OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(logging)
            .addInterceptor { chain ->
                val newUrl = chain.request().url
                    .newBuilder()
                    .build()

                val newRequest = chain.request()
                    .newBuilder()
                    .url(newUrl)
                    .build()

                chain.proceed(newRequest)
            }

        return builder.build()
    }

    private fun retrofit(): Retrofit = Retrofit.Builder()
        .client(getOkHttpClient())
        .baseUrl(DefinitionsApi.DOMAIN)
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(CoroutineCallAdapterFactory()).build()



    override fun getSearchTermResultAsync(queryText: String?, page: Int): Deferred<List<SearchTermModel>> {
        return myJobNowApi.getSearchTermResultsAsync(queryText, page)
    }

    override fun getImageUrlAsync(featuredMediaId: String): Deferred<ImageResponse> {
        return myJobNowApi.getImageUrlAsync(featuredMediaId)
    }

}