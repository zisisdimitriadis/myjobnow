package zisis.mobile.myjobnow.mvp.view.splash

import zisis.example.myjobnow.mvp.view.base.MVPView

interface SplashView : MVPView {

    fun goToLoginScreen()
    fun goToSearchScreen()
}