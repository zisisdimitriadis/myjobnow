package zisis.mobile.myjobnow.mvp.view.login

import zisis.example.myjobnow.mvp.view.base.MVPView

interface LoginView: MVPView  {

    fun showSuccessLogin()
    fun showFailLogin()

}