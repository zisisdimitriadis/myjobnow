package zisis.mobile.myjobnow.mvp.presenter.search

import zisis.mobile.myjobnow.mvp.interactor.search.SearchTermInteractor
import zisis.mobile.myjobnow.mvp.presenter.base.MVPPresenter
import zisis.mobile.myjobnow.mvp.view.search.SearchTermView

interface SearchTermPresenter: MVPPresenter<SearchTermView, SearchTermInteractor> {

    fun getSuggestions(query: String)
    fun logout()
}