package zisis.mobile.myjobnow.mvp.interactor.splash

import zisis.example.weatherdemo.mvp.interactor.base.BaseInteractor
import zisis.mobile.myjobnow.common.utils.preferences.AppSharedPreferences

class SplashInteractorImpl(private val sharedPreferences: AppSharedPreferences) : BaseInteractor(),
    SplashInteractor {

    override fun isUserLoggedIn(): Boolean {
       return sharedPreferences.isUserLoggedIn()
    }
}