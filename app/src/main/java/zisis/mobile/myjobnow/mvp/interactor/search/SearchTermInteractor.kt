package zisis.mobile.myjobnow.mvp.interactor.search

import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.auth.FirebaseAuth
import zisis.mobile.myjobnow.models.DataResult
import zisis.mobile.myjobnow.mvp.interactor.base.MVPInteractor
import zisis.mobile.myjobnow.network.parsers.search.SearchTermModel
import zisis.mobile.myjobnow.network.parsers.search.SearchTermResponse
import zisis.mobile.myjobnow.network.parsers.search.image.ImageResponse

interface SearchTermInteractor: MVPInteractor{

    suspend fun getSuggestions(query: String?, page: Int): DataResult<List<SearchTermModel>>
    suspend fun getImageUrl(featuredMediaId: Long) : DataResult<ImageResponse>
    fun logout()

}