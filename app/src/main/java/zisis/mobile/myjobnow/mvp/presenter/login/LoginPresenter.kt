package zisis.mobile.myjobnow.mvp.presenter.login

import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import zisis.mobile.myjobnow.mvp.interactor.login.LoginInteractor
import zisis.mobile.myjobnow.mvp.presenter.base.MVPPresenter
import zisis.mobile.myjobnow.mvp.view.login.LoginView

interface LoginPresenter: MVPPresenter<LoginView, LoginInteractor> {

    fun login(account: GoogleSignInAccount?)
}