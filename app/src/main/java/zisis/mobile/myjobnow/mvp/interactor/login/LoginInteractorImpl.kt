package zisis.mobile.myjobnow.mvp.interactor.login

import android.app.Activity
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.Tasks
import com.google.android.gms.tasks.Tasks.await
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import timber.log.Timber
import zisis.example.weatherdemo.mvp.interactor.base.BaseInteractor
import zisis.mobile.myjobnow.common.utils.preferences.AppSharedPreferences
import zisis.mobile.myjobnow.models.DataResult
import zisis.mobile.myjobnow.mvp.view.login.LoginView
import zisis.mobile.myjobnow.ui.activity.login.LoginActivity

class LoginInteractorImpl(private val sharedPreferences: AppSharedPreferences): BaseInteractor(), LoginInteractor {

    override suspend fun firebaseAuthWithGoogle(firebaseAuth: FirebaseAuth, account: GoogleSignInAccount?): DataResult<Boolean> {
       return try {
           val credential = GoogleAuthProvider.getCredential(account?.idToken, null)
           val response = await(firebaseAuth.signInWithCredential(credential))
           return if (response.user != null) {
               sharedPreferences.setUserLoggedIn(true)
               DataResult(true)

           } else{
               DataResult(false)
           }

       } catch (t: Throwable) {
           Timber.d(t)
          DataResult(throwable = t)
       }
    }


}