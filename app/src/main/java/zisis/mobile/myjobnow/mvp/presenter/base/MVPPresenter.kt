package zisis.mobile.myjobnow.mvp.presenter.base

import zisis.mobile.myjobnow.mvp.interactor.base.MVPInteractor
import zisis.example.myjobnow.mvp.view.base.MVPView

interface MVPPresenter<out V: MVPView, out I: MVPInteractor> {
    fun detach()
    fun getView(): V?
    fun getInteractor(): I?
    fun isViewAttached() : Boolean
}