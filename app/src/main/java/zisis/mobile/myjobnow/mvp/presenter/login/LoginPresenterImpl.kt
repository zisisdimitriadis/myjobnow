package zisis.mobile.myjobnow.mvp.presenter.login

import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.firebase.auth.FirebaseAuth
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import zisis.mobile.myjobnow.mvp.interactor.login.LoginInteractor
import zisis.mobile.myjobnow.mvp.presenter.base.BasePresenter
import zisis.mobile.myjobnow.mvp.view.login.LoginView
import zisis.mobile.myjobnow.ui.activity.login.LoginActivity

class LoginPresenterImpl(
    view: LoginView,
    interactor: LoginInteractor,
    private val firebaseAuth: FirebaseAuth
) : BasePresenter<LoginView, LoginInteractor>(view, interactor), LoginPresenter {

    override fun login(account: GoogleSignInAccount?) {
        if (!isViewAttached()) {
            return
        }
        getView()?.showLoader()
        uiScope.launch {
            val response = withContext(bgDispatcher) {
                getInteractor()?.firebaseAuthWithGoogle(firebaseAuth, account)
            }
            if (!isViewAttached()) {
                return@launch
            }
            response?.data?.let {
                if (it) {
                    getView()?.showSuccessLogin()
                } else {
                    getView()?.showFailLogin()
                }
            } ?: kotlin.run {
                getView()?.showGenericError()
            }
        }
        getView()?.hideLoader()
    }
}