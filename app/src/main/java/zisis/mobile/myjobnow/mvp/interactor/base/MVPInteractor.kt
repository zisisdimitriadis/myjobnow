package zisis.mobile.myjobnow.mvp.interactor.base

interface MVPInteractor {
    fun detach()
}