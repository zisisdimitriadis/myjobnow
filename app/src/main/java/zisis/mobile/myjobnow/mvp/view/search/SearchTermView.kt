package zisis.mobile.myjobnow.mvp.view.search

import zisis.example.myjobnow.mvp.view.base.MVPView
import zisis.mobile.myjobnow.network.parsers.search.SearchTermModel

interface SearchTermView : MVPView{

    fun showSuggestions(suggestionsList: ArrayList<SearchTermModel>, finishPagination: Boolean)
    fun logout()
}