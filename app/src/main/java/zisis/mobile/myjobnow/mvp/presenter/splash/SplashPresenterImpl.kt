package zisis.mobile.myjobnow.mvp.presenter.splash

import zisis.mobile.myjobnow.mvp.interactor.splash.SplashInteractor
import zisis.mobile.myjobnow.mvp.presenter.base.BasePresenter
import zisis.mobile.myjobnow.mvp.view.splash.SplashView

class SplashPresenterImpl(view: SplashView, interactor: SplashInteractor) :
    BasePresenter<SplashView, SplashInteractor>(view, interactor), SplashPresenter {


    override fun goToNextScreen() {
        if (!isViewAttached()) {
            return
        }

        if (getInteractor()?.isUserLoggedIn() == true) {
            getView()?.goToSearchScreen()
        } else {
            getView()?.goToLoginScreen()
        }
    }
}