package zisis.mobile.myjobnow.mvp.interactor.login

import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import zisis.mobile.myjobnow.mvp.interactor.base.MVPInteractor
import zisis.mobile.myjobnow.models.DataResult

interface LoginInteractor : MVPInteractor {

    suspend fun firebaseAuthWithGoogle(firebaseAuth: FirebaseAuth, account: GoogleSignInAccount?): DataResult<Boolean>
}