package zisis.mobile.myjobnow.mvp.presenter.search

import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import zisis.mobile.myjobnow.mvp.interactor.search.SearchTermInteractor
import zisis.mobile.myjobnow.mvp.presenter.base.BasePresenter
import zisis.mobile.myjobnow.mvp.view.search.SearchTermView
import zisis.mobile.myjobnow.network.parsers.search.SearchTermModel
import zisis.mobile.myjobnow.network.parsers.search.SearchTermResponse

class SearchTermPresenterImpl(
    view: SearchTermView,
    interactor: SearchTermInteractor
) : BasePresenter<SearchTermView, SearchTermInteractor>(view, interactor), SearchTermPresenter {

    companion object {
        private const val MIN_QUERY_LENGTH = 4
        private const val PAGINATION_SIZE = 10
    }

    private var pageIndex = 0
    private var isFetching = false
    private var searchTermModelList: ArrayList<SearchTermModel> = arrayListOf()


    override fun getSuggestions(query: String) {
        if (!isViewAttached()) {
            return
        }

        if (query.trim().length < MIN_QUERY_LENGTH || query.isEmpty()) {
            reset()
            return
        }

        if (isFetching()) {
            return
        }
        setFetching(true)
        pageIndex++
        if (pageIndex == 1) {
            getView()?.showLoader()
        }
        uiScope.launch {
            val response = withContext(bgDispatcher) {
                getInteractor()?.getSuggestions(query, pageIndex)
            }
            if (!isViewAttached()) {
                return@launch
            }

            response?.data?.let { termModelList ->
                if (!termModelList.isNullOrEmpty()) {
                    termModelList.forEach { termModel ->
                        val imageResponse = withContext(bgDispatcher) {
                            termModel.featured_media?.let { getInteractor()?.getImageUrl(it) }
                        }
                        termModel.imageUrl = imageResponse?.data?.media_details?.sizes?.thumbnail?.source_url
                    }
                }
                getView()?.hideLoader()
                searchTermModelList.addAll(termModelList)

                if (searchTermModelList.isNullOrEmpty()) {
                    getView()?.showEmpty()
                    getView()?.hideLoader()
                } else {
                    getView()?.showSuggestions(
                        searchTermModelList,
                        isPaginationFinished(termModelList)
                    )
                    getView()?.hideLoader()
                }
            } ?: response?.throwable?.let {
                getView()?.showGenericError()
                getView()?.hideLoader()
            }
            setFetching(false)
        }

    }

    override fun logout() {
        if (!isViewAttached()) {
            return
        }

        getInteractor()?.logout()
        getView()?.logout()

    }


    @Synchronized
    private fun setFetching(isFetching: Boolean) {
        this.isFetching = isFetching
    }

    @Synchronized
    private fun isFetching(): Boolean {
        return this.isFetching
    }


    private fun reset() {
        pageIndex = 0
        searchTermModelList = arrayListOf()
    }

    private fun isPaginationFinished(termsList: List<SearchTermModel>): Boolean {
        return termsList.isNullOrEmpty() || termsList.size < PAGINATION_SIZE
    }
}