package zisis.mobile.myjobnow.mvp.presenter.splash

import zisis.mobile.myjobnow.mvp.interactor.splash.SplashInteractor
import zisis.mobile.myjobnow.mvp.presenter.base.MVPPresenter
import zisis.mobile.myjobnow.mvp.view.splash.SplashView

interface SplashPresenter: MVPPresenter<SplashView, SplashInteractor> {

    fun goToNextScreen()
}