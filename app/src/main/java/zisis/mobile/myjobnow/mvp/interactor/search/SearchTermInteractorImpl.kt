package zisis.mobile.myjobnow.mvp.interactor.search

import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.tasks.Tasks
import com.google.firebase.auth.FirebaseAuth
import timber.log.Timber
import zisis.example.weatherdemo.mvp.interactor.base.BaseInteractor
import zisis.mobile.myjobnow.common.utils.preferences.AppSharedPreferences
import zisis.mobile.myjobnow.models.DataResult
import zisis.mobile.myjobnow.network.parsers.search.SearchTermModel
import zisis.mobile.myjobnow.network.parsers.search.SearchTermResponse
import zisis.mobile.myjobnow.network.parsers.search.image.ImageResponse
import zisis.mobile.myjobnow.providers.NetworkProvider

class SearchTermInteractorImpl(private val networkProvider: NetworkProvider, private val sharedPreferences: AppSharedPreferences): BaseInteractor(), SearchTermInteractor {

    override suspend fun getSuggestions(query: String?, page: Int): DataResult<List<SearchTermModel>> {
        return try {
            val response = networkProvider.getSearchTermResultAsync(query, page).await()
            DataResult(response)
        } catch (t: Throwable) {
            Timber.d(t)
            DataResult(throwable = t)
        }
    }

    override suspend fun getImageUrl(featuredMediaId: Long): DataResult<ImageResponse> {
       return try {
           val response = networkProvider.getImageUrlAsync(featuredMediaId.toString()).await()
           DataResult(response)

       }catch (t: Throwable) {
           Timber.d(t)
           DataResult(throwable = t)
       }
    }

    override fun logout() {
        sharedPreferences.clearData()
    }
}