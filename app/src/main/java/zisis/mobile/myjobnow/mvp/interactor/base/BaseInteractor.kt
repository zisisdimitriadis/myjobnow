package zisis.example.weatherdemo.mvp.interactor.base

import zisis.mobile.myjobnow.mvp.interactor.base.MVPInteractor


open class BaseInteractor: MVPInteractor {
    override fun detach() {}
}