package zisis.mobile.myjobnow.mvp.interactor.splash

import zisis.mobile.myjobnow.mvp.interactor.base.MVPInteractor

interface SplashInteractor: MVPInteractor {

    fun isUserLoggedIn() : Boolean
}