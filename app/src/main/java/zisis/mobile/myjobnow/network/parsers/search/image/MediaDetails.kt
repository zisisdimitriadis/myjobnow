package zisis.mobile.myjobnow.network.parsers.search.image

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class MediaDetails(
    @SerializedName("sizes") val sizes: Sizes? = null
): Parcelable {
}