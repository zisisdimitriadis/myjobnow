package zisis.mobile.myjobnow.network.parsers.search

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SearchTermModel(
    @SerializedName("id") val id: Long? = null,
    @SerializedName("date") val date: String? = null,
    @SerializedName("link") val link: String? = null,
    @SerializedName("title") val title: Title? = null,
    @SerializedName("featured_media") val featured_media : Long? = null,
    var imageUrl : String? = null

): Parcelable{

    fun getTitle(): String {
        return title?.rendered ?: ""
    }
}