package zisis.mobile.myjobnow.network.parsers.search.image

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Sizes(
    @SerializedName("thumbnail") val thumbnail: Thumbnail? = null,
    @SerializedName("medium") val medium: Medium? = null,
    @SerializedName("medium_large") val medium_large: MediumLarge? = null
): Parcelable {}
