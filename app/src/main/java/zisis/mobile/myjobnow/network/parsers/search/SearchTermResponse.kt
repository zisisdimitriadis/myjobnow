package zisis.mobile.myjobnow.network.parsers.search

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SearchTermResponse(
    @SerializedName("data") val data: Data? = null,
    var searchTermModelList: List<SearchTermModel>? = arrayListOf()) :
    Parcelable {

    fun isSuccess(): Boolean {
        return data == null
    }
}