package zisis.mobile.myjobnow.network.parsers.search.image

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ImageResponse(
    @SerializedName("id") val id : Long? = null,
    @SerializedName("guid") val guid: Guid? = null,
    @SerializedName("media_details") val media_details: MediaDetails? = null

): Parcelable {
}