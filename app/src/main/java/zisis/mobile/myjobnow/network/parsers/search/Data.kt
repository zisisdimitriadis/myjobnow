package zisis.mobile.myjobnow.network.parsers.search

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Data(
    @SerializedName("status") val status: Int? = null
): Parcelable {
}