package zisis.mobile.myjobnow.common.application

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex
import zisis.mobile.myjobnow.common.delegates.ApplicationDelegate
import zisis.mobile.myjobnow.common.utils.locale.LocaleUtils
import zisis.mobile.myjobnow.common.utils.preferences.AppSharedPreferences
import zisis.mobile.myjobnow.providers.NetworkProvider
import zisis.mobile.myjobnow.providers.api.MyJobNowClient

class MyJobNowApplication : Application() {

    companion object {
        private lateinit var instance: MyJobNowApplication

        @JvmStatic
        fun get(): MyJobNowApplication {
            return instance
        }
    }

    val appSharedPreferences: AppSharedPreferences by lazy {
        return@lazy AppSharedPreferences(this)
    }

    val networkProvider: NetworkProvider by lazy {
        return@lazy MyJobNowClient(this)
    }

    private val applicationDelegate = ApplicationDelegate(this)

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(LocaleUtils.applyLanguage(base))
        MultiDex.install(this)
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
        applicationDelegate.initTimber()
    }

}