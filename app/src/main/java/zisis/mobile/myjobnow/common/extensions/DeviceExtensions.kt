package zisis.mobile.myjobnow.common.extensions

import android.annotation.SuppressLint
import android.app.Activity
import android.view.inputmethod.InputMethodManager
import timber.log.Timber

@SuppressLint("WrongConstant")
fun Activity.closeSoftKeyboard() {
    try {
        val inputManager = getSystemService("input_method") as InputMethodManager
        currentFocus?.windowToken?.apply {
            inputManager.hideSoftInputFromWindow(this, InputMethodManager.HIDE_NOT_ALWAYS)
        }
    } catch (e: Exception) {
        Timber.d(e)
    }
}
