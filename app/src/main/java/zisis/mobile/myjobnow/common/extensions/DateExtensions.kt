package zisis.mobile.myjobnow.common.extensions

import zisis.mobile.myjobnow.common.extensions.DateFormats.DATE_FORMAT_SEARCH_TERM
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

object DateFormats {
    const val DATE_FORMAT_SEARCH_TERM = "yyyy-MM-dd'T'hh:mm:ss"
}


fun String.toDayMonthYear(): String? {
    return try {
        val formatter = SimpleDateFormat(DATE_FORMAT_SEARCH_TERM, Locale.getDefault())
        val date: Date
        date = formatter.parse(this)
        val dayFormatted: DateFormat = SimpleDateFormat("dd-MM-yyyy")
        dayFormatted.format(date)
    } catch (e: ParseException) {
        e.printStackTrace()
    } as String?
}
