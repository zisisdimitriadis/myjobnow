package zisis.mobile.myjobnow.common.extensions

import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.widget.AppCompatImageView
import com.google.android.material.textfield.TextInputEditText
import com.squareup.picasso.Picasso
import kotlinx.coroutines.*
import zisis.mobile.myjobnow.R

fun TextInputEditText.afterTextChangedDebounce(delayMillis: Long, input: (String) -> Unit, isTyping: (Boolean) -> Unit) {
    var lastInput = ""
    var debounceJob: Job? = null
    val uiScope = CoroutineScope(Dispatchers.Main + SupervisorJob())
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            if (editable != null) {
                val newtInput = editable.toString()
                isTyping(newtInput.isNotEmpty())
                debounceJob?.cancel()
                if (lastInput != newtInput) {
                    lastInput = newtInput
                    debounceJob = uiScope.launch {
                        delay(delayMillis)
                        if (lastInput == newtInput) {
                            input(newtInput)
                        }
                    }
                }
            }
        }

        override fun beforeTextChanged(cs: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(cs: CharSequence?, start: Int, before: Int, count: Int) {}
    })
}

fun AppCompatImageView.loadUrl(url: String?, placeHolder: Int? = null, errorImageRes: Int = R.mipmap.ic_launcher) {
    if (url.isNullOrEmpty()) {
        Picasso.get().load(errorImageRes).placeholder(errorImageRes).into(this)
        return
    }
    if (placeHolder != null) {
        Picasso.get().load(url).placeholder(placeHolder).into(this)
        return
    }
    Picasso.get().load(url).into(this)
}
