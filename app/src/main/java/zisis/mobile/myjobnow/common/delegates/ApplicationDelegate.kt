package zisis.mobile.myjobnow.common.delegates

import android.app.Application
import timber.log.Timber
import zisis.mobile.myjobnow.BuildConfig
import zisis.mobile.myjobnow.common.delegates.base.BaseDelegate


class ApplicationDelegate(
    application: Application
) : BaseDelegate<Application>(application) {

    fun initTimber() {
        if (BuildConfig.DEBUG)
            Timber.plant(Timber.DebugTree())
    }

}