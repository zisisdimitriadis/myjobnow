package zisis.mobile.myjobnow.common.delegates.base

import java.lang.ref.WeakReference

open class BaseDelegate<T> {
    protected var weakReference: WeakReference<T>?

    constructor(reference: T) {
        weakReference = WeakReference(reference)
    }

    open fun attach(reference: T) {
        weakReference?.clear()
        weakReference = WeakReference(reference)
    }

    open fun detach() {
        weakReference?.clear()
        weakReference = null
    }

    open fun isAttached(): Boolean {
        return weakReference != null && weakReference?.get() != null
    }

    fun getReference(): T? {
        return weakReference?.get()
    }
}