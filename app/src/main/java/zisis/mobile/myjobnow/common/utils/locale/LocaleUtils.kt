package zisis.mobile.myjobnow.common.utils.locale

import android.content.Context
import android.os.Build
import java.util.*


object LocaleUtils {

    @SuppressWarnings("Deprecated in Android 17")
    fun applyLanguage(context: Context): Context {
        val configuration = context.resources.configuration
        val displayMetrics = context.resources.displayMetrics
        val current: Locale = getCurrentLocale(context)!!

        //Locale.setDefault(current)

        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            configuration.setLocale(current)
            context.createConfigurationContext(configuration)
        } else {
            configuration.locale = current
            context.resources.updateConfiguration(configuration, displayMetrics)
            context
        }
    }

    private fun getCurrentLocale(context: Context): Locale? {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            context.resources.configuration.locales[0]
        } else {
            context.resources.configuration.locale
        }
    }
}