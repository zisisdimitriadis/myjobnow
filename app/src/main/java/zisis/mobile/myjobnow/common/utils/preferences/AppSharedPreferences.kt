package zisis.mobile.myjobnow.common.utils.preferences

import android.content.Context
import androidx.preference.PreferenceManager
import zisis.mobile.myjobnow.R

class AppSharedPreferences(appContext: Context) {

    val preferences = PreferenceManager.getDefaultSharedPreferences(appContext)

    private val appContext = appContext

    companion object {
        private const val IS_LOGGED_IN = "is_logged_in"
        private const val SHOULD_SHOW_WELCOME_MESSAGE = "should_show_welcome_message"
        private const val WELCOME_REMOTE_MESSAGE_GR = "welcome_remote_message_gr"
        private const val WELCOME_REMOTE_MESSAGE_EN = "welcome_remote_message_en"
    }

    @Synchronized
    fun isUserLoggedIn(): Boolean {
        return preferences.getBoolean((IS_LOGGED_IN), false)
    }

    @Synchronized
    fun setShowWelcomeMessage(value: Boolean) {
        preferences.edit().putBoolean((SHOULD_SHOW_WELCOME_MESSAGE), value).apply()
    }

    @Synchronized
    fun shouldShowWelcomeMessage(): Boolean {
        return preferences.getBoolean((SHOULD_SHOW_WELCOME_MESSAGE), true)
    }

    @Synchronized
    fun setUserLoggedIn(value: Boolean) {
        preferences.edit().putBoolean((IS_LOGGED_IN), value).apply()
    }

    @Synchronized
    fun setWelcomeMessageValueGR(value: String) {
        preferences.edit().putString((WELCOME_REMOTE_MESSAGE_GR), value).apply()
    }

    @Synchronized
    fun getWelcomeMessageValueGR(): String? {
        return preferences.getString((WELCOME_REMOTE_MESSAGE_GR), "")
    }

    @Synchronized
    fun setWelcomeMessageValueEN(value: String) {
        preferences.edit().putString((WELCOME_REMOTE_MESSAGE_EN), value).apply()
    }

    @Synchronized
    fun getWelcomeMessageValueEN(): String? {
        return preferences.getString((WELCOME_REMOTE_MESSAGE_EN), "")
    }


    @Synchronized
    fun clearData() {
        setUserLoggedIn(value = false)
        setShowWelcomeMessage(value = true)
    }

}