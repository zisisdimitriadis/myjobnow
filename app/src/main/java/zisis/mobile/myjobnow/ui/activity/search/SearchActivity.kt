package zisis.mobile.myjobnow.ui.activity.search

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.abt.FirebaseABTesting
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings
import kotlinx.android.synthetic.main.activity_search.*
import kotlinx.android.synthetic.main.layout_pagination_recycler_view.*
import kotlinx.android.synthetic.main.layout_search_term.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import timber.log.Timber
import zisis.mobile.myjobnow.R
import zisis.mobile.myjobnow.common.application.MyJobNowApplication
import zisis.mobile.myjobnow.common.extensions.afterTextChangedDebounce
import zisis.mobile.myjobnow.common.extensions.closeSoftKeyboard
import zisis.mobile.myjobnow.mvp.interactor.search.SearchTermInteractorImpl
import zisis.mobile.myjobnow.mvp.presenter.search.SearchTermPresenter
import zisis.mobile.myjobnow.mvp.presenter.search.SearchTermPresenterImpl
import zisis.mobile.myjobnow.mvp.view.search.SearchTermView
import zisis.mobile.myjobnow.network.parsers.search.SearchTermModel
import zisis.mobile.myjobnow.ui.activity.base.BaseMVPActivity
import zisis.mobile.myjobnow.ui.activity.details.SearchDetailsActivity
import zisis.mobile.myjobnow.ui.activity.login.LoginActivity
import zisis.mobile.myjobnow.ui.adapter.SearchTermRecyclerViewAdapter
import zisis.mobile.myjobnow.ui.custom.PaginationScrollListener

class SearchActivity : BaseMVPActivity<SearchTermPresenter>(), SearchTermView {

    companion object {
        const val DELAY_MILLIS: Long = 400L
        const val SEARCH_TERM_MODEL = "SearchTermModel"
        private const val PAGINATION_SIZE = 10
    }

    private lateinit var searchTermRecyclerViewAdapter: SearchTermRecyclerViewAdapter
    private var paginationScrollListener: PaginationScrollListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        presenter = SearchTermPresenterImpl(
            this,
            SearchTermInteractorImpl(MyJobNowApplication.get().networkProvider, MyJobNowApplication.get().appSharedPreferences)
        )
        initLayout()
    }

    private fun initLayout() {
        toolbarTextView?.text = getString(R.string.search_screen_toolbar_text)
        backButtonImageView?.setOnClickListener {
            finish()
        }

        displayWelcomeMessage()


        val layoutManager = LinearLayoutManager(this)
        searchRecyclerView?.layoutManager = layoutManager
        paginationScrollListener = PaginationScrollListener(
            layoutManager, {
                moreProgressView?.visibility = View.VISIBLE
                presenter?.getSuggestions(searchEditText.text.toString())
            },
            PAGINATION_SIZE
        )
        paginationScrollListener?.let { paginationScrollListener ->
            searchRecyclerView.addOnScrollListener(paginationScrollListener)
        }
        searchTermRecyclerViewAdapter = SearchTermRecyclerViewAdapter(searchTermClick = {
            //go to details screen
            val detailsIntent = Intent(this@SearchActivity, SearchDetailsActivity::class.java)
            val bundle = Bundle()
            bundle.putParcelable(SEARCH_TERM_MODEL, it)
            startActivityWithTransition(detailsIntent, bundle)
        })

        searchRecyclerView?.adapter = searchTermRecyclerViewAdapter
        searchEditText?.afterTextChangedDebounce(DELAY_MILLIS, { text ->
            searchRecyclerView.visibility = if (text.isEmpty()) View.GONE else View.VISIBLE
            presenter?.getSuggestions(text)
        }, {})

        searchRecyclerView.addOnScrollListener(onKeyboardDismissingScrollListener)

        logoutButton.setOnClickListener {
            presenter?.logout()
        }

    }

    private fun displayWelcomeMessage() {
        if (MyJobNowApplication.get().appSharedPreferences.shouldShowWelcomeMessage()) {
            Toast.makeText(
                this, MyJobNowApplication.get().appSharedPreferences.getWelcomeMessageValueGR(),
                Toast.LENGTH_LONG
            ).show()
        }

        MyJobNowApplication.get().appSharedPreferences.setShowWelcomeMessage(false)
    }

    override fun showSuggestions(
        suggestionsList: ArrayList<SearchTermModel>,
        finishPagination: Boolean
    ) {
        if (suggestionsList.isEmpty()) {
            showEmpty()
            return
        }

        hideEmpty()
        moreProgressView?.visibility = View.GONE
        paginationScrollListener?.finishedPagination(finishPagination)
        searchTermRecyclerViewAdapter.setSuggestionList(suggestionsList)
    }

    override fun logout() {
        FirebaseAuth.getInstance().signOut()
        startActivity(Intent(this, LoginActivity::class.java))
        finish()
        overridePendingTransition(0, R.anim.animation_slide_out_right)
    }

    private val onKeyboardDismissingScrollListener = object : RecyclerView.OnScrollListener() {
        var isKeyboardDismissedByScroll: Boolean = false

        override fun onScrollStateChanged(recyclerView: RecyclerView, state: Int) {
            when (state) {
                RecyclerView.SCROLL_STATE_DRAGGING -> if (!isKeyboardDismissedByScroll) {
                    closeSoftKeyboard()
                    isKeyboardDismissedByScroll = !isKeyboardDismissedByScroll
                }
                RecyclerView.SCROLL_STATE_IDLE -> isKeyboardDismissedByScroll = false
            }
        }
    }
}