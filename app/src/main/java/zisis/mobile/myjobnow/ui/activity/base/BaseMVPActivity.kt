package zisis.mobile.myjobnow.ui.activity.base

import android.view.View
import kotlinx.android.synthetic.main.layout_empty.*
import kotlinx.android.synthetic.main.layout_loading.*
import zisis.mobile.myjobnow.mvp.interactor.base.MVPInteractor
import zisis.example.myjobnow.mvp.view.base.MVPView
import zisis.mobile.myjobnow.R
import zisis.mobile.myjobnow.mvp.presenter.base.MVPPresenter

open class BaseMVPActivity<T : MVPPresenter<MVPView, MVPInteractor>> : BaseActivity(), MVPView {

    protected var presenter: T? = null

    override fun isAttached(): Boolean {
        return !isFinishing
    }

    override fun showLoader() {
        hideEmpty()
        loadingView?.visibility = View.VISIBLE
    }

    override fun hideLoader() {
        loadingView?.visibility = View.GONE
    }

    override fun showEmpty() {
        hideLoader()
        emptyTextView?.text = getString(R.string.empty_view_generic_text)
        emptyView?.visibility = View.VISIBLE
    }

    override fun hideEmpty() {
        emptyView?.visibility = View.GONE
    }

    override fun showError(error: String) {

    }

    override fun showNoInternetError() {
        showDialog(
            getString(R.string.dialog_error_title),
            getString(R.string.no_internet),
            getString(R.string.dialog_error_button)
        )
    }

    override fun showGenericError() {
        showDialog(
            getString(R.string.dialog_error_title),
            getString(R.string.generic_error),
            getString(R.string.dialog_error_button)
        )
    }
}