package zisis.mobile.myjobnow.ui.activity.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import com.afollestad.materialdialogs.DialogCallback
import com.afollestad.materialdialogs.MaterialDialog
import kotlinx.android.synthetic.main.activity_root.*
import zisis.mobile.myjobnow.R
import zisis.mobile.myjobnow.common.utils.locale.LocaleUtils

open class BaseActivity : AppCompatActivity() {

    private var materialDialog: MaterialDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun attachBaseContext(base: Context) {
        super.attachBaseContext(LocaleUtils.applyLanguage(base))
    }

    override fun setContentView(layoutResID: Int) {
        super.setContentView(R.layout.activity_root)
        val layout = layoutInflater.inflate(layoutResID, containerView, false)
        containerView?.addView(layout)
    }

    override fun onDestroy() {
        super.onDestroy()
        materialDialog?.dismiss()
    }

    override fun onResume() {
        super.onResume()
    }

    override fun onPause() {
        super.onPause()
    }



    protected fun initToolbar(toolbar: Toolbar?, isHomeUpEnabled: Boolean, titleToolbar: String?) {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(isHomeUpEnabled)
        supportActionBar?.setDisplayShowHomeEnabled(isHomeUpEnabled)
        supportActionBar?.title = titleToolbar
    }

    fun startActivityWithTransition(intent: Intent) {
        startActivity(intent)
        overridePendingTransition(R.anim.animation_slide_in_right, R.anim.animation_zoom_out)
    }

    fun startActivityWithTransition(intent: Intent, bundle: Bundle) {
        intent.putExtras(bundle)
        startActivity(intent)
        overridePendingTransition(R.anim.animation_slide_in_right, R.anim.animation_zoom_out)
    }

    fun startActivityModal(intent: Intent) {
        startActivity(intent)
        overridePendingTransition(R.anim.animation_slide_up, R.anim.animation_zoom_out)
    }

    fun showDialog(
        titleText: String = "",
        messageText: String = "",
        leftButtonText: String = "",
        rightButtonText: String = "",
        leftButtonListener: DialogCallback? = null,
        rightButtonListener: DialogCallback? = null
    ) {

        materialDialog?.dismiss()
        materialDialog = MaterialDialog(this).show {
            title(text = titleText)
            message(text = messageText)
            positiveButton(text = rightButtonText) {
                rightButtonListener?.invoke(this)
            }
            negativeButton(text = leftButtonText) {
                leftButtonListener?.invoke(this)
            }

        }
    }

}