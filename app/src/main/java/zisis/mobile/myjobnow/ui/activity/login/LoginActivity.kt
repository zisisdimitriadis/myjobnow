package zisis.mobile.myjobnow.ui.activity.login

import android.content.Intent
import android.os.Bundle
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuth.AuthStateListener
import com.google.firebase.auth.GoogleAuthProvider
import kotlinx.android.synthetic.main.activity_login.*
import timber.log.Timber
import zisis.mobile.myjobnow.R
import zisis.mobile.myjobnow.common.application.MyJobNowApplication
import zisis.mobile.myjobnow.mvp.interactor.login.LoginInteractorImpl
import zisis.mobile.myjobnow.mvp.presenter.login.LoginPresenter
import zisis.mobile.myjobnow.mvp.presenter.login.LoginPresenterImpl
import zisis.mobile.myjobnow.mvp.view.login.LoginView
import zisis.mobile.myjobnow.ui.activity.base.BaseActivity
import zisis.mobile.myjobnow.ui.activity.base.BaseMVPActivity
import zisis.mobile.myjobnow.ui.activity.search.SearchActivity


class LoginActivity : BaseMVPActivity<LoginPresenter>(), LoginView {

    private lateinit var firebaseAuth: FirebaseAuth
    private val REQUEST_CODE_SIGN_IN = 1001
    private var mGoogleSignInClient: GoogleSignInClient? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initLayout()
        presenter = LoginPresenterImpl(this, LoginInteractorImpl(MyJobNowApplication.get().appSharedPreferences), FirebaseAuth.getInstance())
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE_SIGN_IN) {
            val task =
                GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account =
                    task.getResult(ApiException::class.java)
                Timber.d("MyJobNow -> Login -> firebaseAuthWithGoogle -> id = %s, name = %s, email = %s", account?.id, account?.displayName, account?.email)
                presenter?.login(account)
            } catch (e: ApiException) {
                Timber.d("MyJobNow -> Login -> Login in Unsuccessful with error -> %s", e.toString())
                showDialog(titleText = getString(R.string.dialog_error_title),
                    messageText = getString(R.string.dialog_login_error),
                    rightButtonText = getString(R.string.dialog_ok_button),
                    rightButtonListener = {
                        it.dismiss()
                    })
            }
        }
    }

    private fun firebaseAuthWithGoogle(account: GoogleSignInAccount?) {
        Timber.d("MyJobNow -> Login -> firebaseAuthWithGoogle -> %s", account?.id)
        //getting the auth credential
        val credential = GoogleAuthProvider.getCredential(account?.idToken, null)
        //Now using firebase we are signing in the user here
        firebaseAuth.signInWithCredential(credential)
            .addOnCompleteListener(this,
                OnCompleteListener<AuthResult?> { task ->
                    if (task.isSuccessful) {
                        Timber.d("MyJobNow -> Login -> firebaseAuthWithGoogle -> signInWithCredential -> success")
                        val user = firebaseAuth.currentUser
                        //TODO: update UI, for example show success message and go to next screen
                        showDialog(messageText = getString(R.string.dialog_login_success_message_text),
                        rightButtonText = getString(R.string.dialog_ok_button),
                        rightButtonListener = {
                            it.dismiss()
                        })
                        Timber.d("MyJobNow -> Login -> firebaseAuthWithGoogle -> signInWithCredential -> success -> user email is %s", user?.email)
                    } else {
                        // If sign in fails, display a message to the user.
                        Timber.d("MyJobNow -> Login -> firebaseAuthWithGoogle -> signInWithCredential -> Authentication failed with error %s", task.exception?.message)
                        showDialog(titleText = getString(R.string.dialog_error_title),
                        messageText = getString(R.string.dialog_login_error),
                        rightButtonText = getString(R.string.dialog_ok_button),
                        rightButtonListener = {
                            it.dismiss()
                        })
                    }
                })


    }

    private fun initLayout() {
        firebaseAuth = FirebaseAuth.getInstance()
        val googleSignInOptions =
            GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.web_client_id))
                .requestEmail()
                .build()
        mGoogleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions);
        loginButton?.setOnClickListener {
            performSignIn()
        }
    }

    private fun performSignIn() {
        startActivityForResult(Intent(mGoogleSignInClient?.signInIntent), REQUEST_CODE_SIGN_IN)
    }

    override fun showSuccessLogin() {
        Timber.d("MyJobNow -> Login -> firebaseAuthWithGoogle -> signInWithCredential -> success")
        showDialog(messageText = getString(R.string.dialog_login_success_message_text),
            rightButtonText = getString(R.string.dialog_ok_button),
            rightButtonListener = {
                it.dismiss()
                finish()
                startActivityWithTransition(Intent(this, SearchActivity::class.java))
            })
    }

    override fun showFailLogin() {
        Timber.d("MyJobNow -> Login -> firebaseAuthWithGoogle -> signInWithCredential -> Authentication failed")
        showDialog(titleText = getString(R.string.dialog_error_title),
            messageText = getString(R.string.dialog_login_error),
            rightButtonText = getString(R.string.dialog_ok_button),
            rightButtonListener = {
                it.dismiss()
            })
    }

}