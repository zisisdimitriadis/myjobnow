package zisis.mobile.myjobnow.ui.activity.details

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import kotlinx.android.synthetic.main.activity_search_details.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import zisis.mobile.myjobnow.R
import zisis.mobile.myjobnow.common.application.MyJobNowApplication
import zisis.mobile.myjobnow.common.extensions.loadUrl
import zisis.mobile.myjobnow.common.extensions.toDayMonthYear
import zisis.mobile.myjobnow.mvp.interactor.search.SearchTermInteractorImpl
import zisis.mobile.myjobnow.mvp.presenter.search.SearchTermPresenterImpl
import zisis.mobile.myjobnow.network.parsers.search.SearchTermModel
import zisis.mobile.myjobnow.ui.activity.base.BaseActivity
import zisis.mobile.myjobnow.ui.adapter.SearchTermRecyclerViewAdapter

class SearchDetailsActivity : BaseActivity() {

    companion object {
        const val SEARCH_TERM_MODEL = "SearchTermModel"
    }

    private lateinit var searchTermModel : SearchTermModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_details)
        getPassData()
        initLayout()
    }


    private fun initLayout() {
        toolbarTextView.text = getString(R.string.details_toolbar_text)
        titleTextView.text = searchTermModel.getTitle()
        dateTextView.text = searchTermModel.date?.toDayMonthYear()
        postImageView.loadUrl(searchTermModel.imageUrl)
        fabButton.setOnClickListener {
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(searchTermModel.link))
            startActivity(browserIntent)
        }

        backButtonImageView.setOnClickListener {
            finish()
            overridePendingTransition(0, R.anim.animation_slide_out_right)
        }
    }


    private fun getPassData() {
        searchTermModel = intent?.extras?.getParcelable(SEARCH_TERM_MODEL) ?: SearchTermModel()
    }
}