package zisis.mobile.myjobnow.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.row_search_term.*
import kotlinx.android.synthetic.main.row_search_term.view.*
import kotlinx.android.synthetic.main.row_search_term.view.termTextView
import zisis.mobile.myjobnow.R
import zisis.mobile.myjobnow.common.extensions.loadUrl
import zisis.mobile.myjobnow.network.parsers.search.SearchTermModel
import zisis.mobile.myjobnow.network.parsers.search.image.ImageResponse

class SearchTermRecyclerViewAdapter(private val searchTermClick: (SearchTermModel) -> Unit) :
    RecyclerView.Adapter<SearchTermRecyclerViewAdapter.ViewHolder>() {

    private var searchTermModelList: ArrayList<SearchTermModel> = arrayListOf()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): SearchTermRecyclerViewAdapter.ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.row_search_term, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return searchTermModelList.size
    }

    override fun onBindViewHolder(holder: SearchTermRecyclerViewAdapter.ViewHolder, position: Int) {
        val searchTermModel = searchTermModelList[position]
        holder.bind(searchTermModel, searchTermClick)
    }

    fun setSuggestionList(list: ArrayList<SearchTermModel>) {
            searchTermModelList = list
            notifyDataSetChanged()
        }

    class ViewHolder(override val containerView: View) : RecyclerView.ViewHolder(containerView),
        LayoutContainer {

        fun bind(searchTermModel: SearchTermModel, searchTermClick: (SearchTermModel) -> Unit) {
            termTextView.text = searchTermModel.getTitle()
            termImageView.loadUrl(searchTermModel.imageUrl)
            itemView.setOnClickListener {
                searchTermClick.invoke(searchTermModel)
            }
        }
    }
}