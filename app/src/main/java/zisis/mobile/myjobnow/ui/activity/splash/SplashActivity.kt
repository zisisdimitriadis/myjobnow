package zisis.mobile.myjobnow.ui.activity.splash

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.animation.AnimationUtils
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import kotlinx.android.synthetic.main.activity_splash.*
import timber.log.Timber
import zisis.mobile.myjobnow.R
import zisis.mobile.myjobnow.common.application.MyJobNowApplication
import zisis.mobile.myjobnow.mvp.interactor.splash.SplashInteractorImpl
import zisis.mobile.myjobnow.mvp.presenter.splash.SplashPresenter
import zisis.mobile.myjobnow.mvp.presenter.splash.SplashPresenterImpl
import zisis.mobile.myjobnow.mvp.view.splash.SplashView
import zisis.mobile.myjobnow.ui.activity.base.BaseMVPActivity
import zisis.mobile.myjobnow.ui.activity.login.LoginActivity
import zisis.mobile.myjobnow.ui.activity.search.SearchActivity

class SplashActivity : BaseMVPActivity<SplashPresenter>(), SplashView {

    companion object {
        const val SPLASH_DELAY_IN_MILLIS = 3000L
    }

    private lateinit var remoteConfig: FirebaseRemoteConfig

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)
        presenter = SplashPresenterImpl(
            this,
            SplashInteractorImpl(MyJobNowApplication.get().appSharedPreferences)
        )
        initLayout()
    }

    private fun initLayout() {
        remoteConfig = FirebaseRemoteConfig.getInstance()
        val animationFirstText = AnimationUtils.loadAnimation(this, R.anim.animation_slide_down)
        firstTextView.startAnimation(animationFirstText)

        val animationSecondText =
            AnimationUtils.loadAnimation(this, R.anim.animation_slide_in_right_splash)
        secondTextView.startAnimation(animationSecondText)

        val animationThirdText = AnimationUtils.loadAnimation(this, R.anim.animation_slide_up)
        thirdTextView.startAnimation(animationThirdText)

        //fetchRemoteConfigAndGoToNextScreen()

        Handler().postDelayed(
            {
                fetchRemoteConfigAndGoToNextScreen()
            }, SPLASH_DELAY_IN_MILLIS
        )
    }

    override fun goToLoginScreen() {
        val homeIntent = Intent(this, LoginActivity::class.java)
        startActivityWithTransition(homeIntent)
        finish()
    }

    override fun goToSearchScreen() {
        val homeIntent = Intent(this, SearchActivity::class.java)
        startActivityWithTransition(homeIntent)
        finish()
    }

    private fun fetchRemoteConfigAndGoToNextScreen() {
        showLoader()
        remoteConfig.fetchAndActivate()
            .addOnCompleteListener(this) { task ->
                if (task.isSuccessful) {
                    val updated = task.result
                    Timber.d("MyJobNow -> Config params updated: $updated")
                    Timber.d("MyJobNow -> Config params fetched successfully")
                    MyJobNowApplication.get().appSharedPreferences.setWelcomeMessageValueGR(
                        remoteConfig.getString("welcome_message_gr")
                    )
                    MyJobNowApplication.get().appSharedPreferences.setWelcomeMessageValueEN(
                        remoteConfig.getString("welcome_message_en")
                    )
                    hideLoader()
                    presenter?.goToNextScreen()

                } else {
                    Timber.d("MyJobNow -> Config params failed to be fetched")
                    hideLoader()
                    presenter?.goToNextScreen()
                }
            }

    }


}