[
  {
    "id": 1701,
    "date": "2019-11-14T18:04:13",
    "date_gmt": "2019-11-14T15:04:13",
    "guid": {
      "rendered": "http:\/\/www.nickagas.com\/?p=1701"
    },
    "modified": "2020-05-29T14:38:43",
    "modified_gmt": "2020-05-29T11:38:43",
    "slug": "android-detekt",
    "status": "publish",
    "type": "post",
    "link": "https:\/\/www.nickagas.com\/android-detekt\/",
    "title": {
      "rendered": "Android detekt"
    },
    "content": {
      "rendered": "",
      "protected": false
    },
    "excerpt": {
      "rendered": "<p>Static code analysis for Kotlin&nbsp;https:\/\/arturbosch.github.io\/detekt\/ Add plugins { id(&#8220;io.gitlab.arturbosch.detekt&#8221;).version(&#8220;[version]&#8221;) } detekt { toolVersion = &#8220;[version]&#8221; input = files(&#8220;src\/main\/kotlin&#8221;) filters = &#8220;.*\/resources\/.*,.*\/build\/.*&#8221; baseline = file(&#8220;my-detekt-baseline.xml&#8221;) \/\/ Just if you want to create a baseline file. } add to app apply plugin: &#8216;io.gitlab.arturbosch.detekt&#8217; gradle -&gt;Tasks-&gt; verification -&gt; detekt -&gt; detektGenerateConfig genetates a file config\/detekt\/detekt.yml and then detekt [&hellip;]<\/p>\n",
      "protected": false
    },
    "author": 1,
    "featured_media": 1841,
    "comment_status": "open",
    "ping_status": "open",
    "sticky": false,
    "template": "",
    "format": "standard",
    "meta": [
      
    ],
    "categories": [
      22,
      27
    ],
    "tags": [
      
    ],
    "_links": {
      "self": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1701"
        }
      ],
      "collection": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts"
        }
      ],
      "about": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/types\/post"
        }
      ],
      "author": [
        {
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/users\/1"
        }
      ],
      "replies": [
        {
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/comments?post=1701"
        }
      ],
      "version-history": [
        {
          "count": 3,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1701\/revisions"
        }
      ],
      "predecessor-version": [
        {
          "id": 1842,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1701\/revisions\/1842"
        }
      ],
      "wp:featuredmedia": [
        {
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/media\/1841"
        }
      ],
      "wp:attachment": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/media?parent=1701"
        }
      ],
      "wp:term": [
        {
          "taxonomy": "category",
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/categories?post=1701"
        },
        {
          "taxonomy": "post_tag",
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/tags?post=1701"
        }
      ],
      "curies": [
        {
          "name": "wp",
          "href": "https:\/\/api.w.org\/{rel}",
          "templated": true
        }
      ]
    }
  },
  {
    "id": 1566,
    "date": "2019-05-14T14:06:24",
    "date_gmt": "2019-05-14T11:06:24",
    "guid": {
      "rendered": "http:\/\/www.nickagas.com\/?p=1566"
    },
    "modified": "2019-05-14T14:06:29",
    "modified_gmt": "2019-05-14T11:06:29",
    "slug": "jenkins-for-android",
    "status": "publish",
    "type": "post",
    "link": "https:\/\/www.nickagas.com\/jenkins-for-android\/",
    "title": {
      "rendered": "Jenkins for Android"
    },
    "content": {
      "rendered": "",
      "protected": false
    },
    "excerpt": {
      "rendered": "<p>Jenkins Build great things at any scale The leading open source automation server, Jenkins provides hundreds of plugins to support building, deploying and automating any project.DocumentationDownload Download and run Jenkins Download Jenkins. Open up a terminal in the download directory. Run&nbsp;java -jar jenkins.war &#8211;httpPort=8080. Browse to&nbsp;http:\/\/localhost:8080. Follow the instructions to complete the installation. https:\/\/www.vogella.com\/tutorials\/JenkinsAndroid\/article.html https:\/\/github.com\/shadowmanpat\/NewJenkinsProject [&hellip;]<\/p>\n",
      "protected": false
    },
    "author": 1,
    "featured_media": 1570,
    "comment_status": "open",
    "ping_status": "open",
    "sticky": false,
    "template": "",
    "format": "standard",
    "meta": [
      
    ],
    "categories": [
      22,
      27,
      21,
      35,
      2
    ],
    "tags": [
      
    ],
    "_links": {
      "self": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1566"
        }
      ],
      "collection": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts"
        }
      ],
      "about": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/types\/post"
        }
      ],
      "author": [
        {
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/users\/1"
        }
      ],
      "replies": [
        {
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/comments?post=1566"
        }
      ],
      "version-history": [
        {
          "count": 5,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1566\/revisions"
        }
      ],
      "predecessor-version": [
        {
          "id": 1572,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1566\/revisions\/1572"
        }
      ],
      "wp:featuredmedia": [
        {
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/media\/1570"
        }
      ],
      "wp:attachment": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/media?parent=1566"
        }
      ],
      "wp:term": [
        {
          "taxonomy": "category",
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/categories?post=1566"
        },
        {
          "taxonomy": "post_tag",
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/tags?post=1566"
        }
      ],
      "curies": [
        {
          "name": "wp",
          "href": "https:\/\/api.w.org\/{rel}",
          "templated": true
        }
      ]
    }
  },
  {
    "id": 1561,
    "date": "2019-05-13T07:46:21",
    "date_gmt": "2019-05-13T04:46:21",
    "guid": {
      "rendered": "https:\/\/www.nickagas.com\/?p=1561"
    },
    "modified": "2019-05-13T12:24:35",
    "modified_gmt": "2019-05-13T09:24:35",
    "slug": "angularjs-project",
    "status": "publish",
    "type": "post",
    "link": "https:\/\/www.nickagas.com\/angularjs-project\/",
    "title": {
      "rendered": "AngularJS project"
    },
    "content": {
      "rendered": "<h3><strong>AngularJS for Beginners, Single-Page Applications Made Easy<\/strong><\/h3>\n",
      "protected": false
    },
    "excerpt": {
      "rendered": "<p>AngularJS for Beginners, Single-Page Applications Made Easy http:\/\/agascompany.cuccfree.org\/angular\/ https:\/\/github.com\/shadowmanpat\/AngularJS https:\/\/www.udemy.com\/share\/100SIECUMfdlxbRw==\/ In this course, you will learn how to use Angular.js from scratch so that you can create more user-friendly web applications, Single Page Applications (SPA), and interactive websites. This course is taught by\u00a0Pablo Farias Navarro, founder of\u00a0ZENVA\u00a0and one of Udemy&#8217;s Best-Selling instructors,\u00a0with +50,000 students and [&hellip;]<\/p>\n",
      "protected": false
    },
    "author": 1,
    "featured_media": 0,
    "comment_status": "open",
    "ping_status": "open",
    "sticky": false,
    "template": "",
    "format": "standard",
    "meta": [
      
    ],
    "categories": [
      34
    ],
    "tags": [
      
    ],
    "_links": {
      "self": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1561"
        }
      ],
      "collection": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts"
        }
      ],
      "about": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/types\/post"
        }
      ],
      "author": [
        {
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/users\/1"
        }
      ],
      "replies": [
        {
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/comments?post=1561"
        }
      ],
      "version-history": [
        {
          "count": 2,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1561\/revisions"
        }
      ],
      "predecessor-version": [
        {
          "id": 1565,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1561\/revisions\/1565"
        }
      ],
      "wp:attachment": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/media?parent=1561"
        }
      ],
      "wp:term": [
        {
          "taxonomy": "category",
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/categories?post=1561"
        },
        {
          "taxonomy": "post_tag",
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/tags?post=1561"
        }
      ],
      "curies": [
        {
          "name": "wp",
          "href": "https:\/\/api.w.org\/{rel}",
          "templated": true
        }
      ]
    }
  },
  {
    "id": 1541,
    "date": "2019-03-05T19:56:44",
    "date_gmt": "2019-03-05T16:56:44",
    "guid": {
      "rendered": "http:\/\/www.nickagas.com\/?p=1541"
    },
    "modified": "2019-03-05T19:56:44",
    "modified_gmt": "2019-03-05T16:56:44",
    "slug": "easy-parcelable-in-kotlin",
    "status": "publish",
    "type": "post",
    "link": "https:\/\/www.nickagas.com\/easy-parcelable-in-kotlin\/",
    "title": {
      "rendered": "Easy Parcelable in Kotlin"
    },
    "content": {
      "rendered": "<p>https:\/\/medium.com\/the-lazy-coders-journa",
      "protected": false
    },
    "excerpt": {
      "rendered": "<p>https:\/\/medium.com\/the-lazy-coders-journal\/easy-parcelable-in-kotlin-the-lazy-coders-way-9683122f4c00 Parcelable: The lazy coder\u2019s\u00a0way @Parcelize class Item( var imageId: Int, var title: String, var details: String, var price: Double, var category: Category, var postedOn: Long ) : Parcelable See, easy as that! All you need is just to confirm followings: Use\u00a0@Parcelize\u00a0annotation on top of your Model \/ Data class Use latest version of Kotlin [&hellip;]<\/p>\n",
      "protected": false
    },
    "author": 1,
    "featured_media": 0,
    "comment_status": "open",
    "ping_status": "open",
    "sticky": false,
    "template": "",
    "format": "standard",
    "meta": [
      
    ],
    "categories": [
      22,
      27,
      35
    ],
    "tags": [
      
    ],
    "_links": {
      "self": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1541"
        }
      ],
      "collection": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts"
        }
      ],
      "about": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/types\/post"
        }
      ],
      "author": [
        {
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/users\/1"
        }
      ],
      "replies": [
        {
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/comments?post=1541"
        }
      ],
      "version-history": [
        {
          "count": 1,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1541\/revisions"
        }
      ],
      "predecessor-version": [
        {
          "id": 1542,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1541\/revisions\/1542"
        }
      ],
      "wp:attachment": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/media?parent=1541"
        }
      ],
      "wp:term": [
        {
          "taxonomy": "category",
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/categories?post=1541"
        },
        {
          "taxonomy": "post_tag",
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/tags?post=1541"
        }
      ],
      "curies": [
        {
          "name": "wp",
          "href": "https:\/\/api.w.org\/{rel}",
          "templated": true
        }
      ]
    }
  },
  {
    "id": 1463,
    "date": "2018-11-03T11:51:09",
    "date_gmt": "2018-11-03T08:51:09",
    "guid": {
      "rendered": "http:\/\/www.nickagas.com\/?p=1463"
    },
    "modified": "2019-09-05T13:08:35",
    "modified_gmt": "2019-09-05T10:08:35",
    "slug": "flutter-devlopment",
    "status": "publish",
    "type": "post",
    "link": "https:\/\/www.nickagas.com\/flutter-devlopment\/",
    "title": {
      "rendered": "Flutter devlopment"
    },
    "content": {
      "rendered": "",
      "protected": false
    },
    "excerpt": {
      "rendered": "<p>Build beautiful native apps in&nbsp;record&nbsp;time Flutter is Google\u2019s mobile app SDK for crafting high-quality native interfaces on iOS and Android in record time. Flutter works with existing code, is used by developers and organizations around the world, and is free and open source. GET STARTED links https:\/\/flutter.io\/ https:\/\/github.com\/Solido\/awesome-flutter https:\/\/github.com\/iampawan\/FlutterExampleApps https:\/\/developers.google.com\/events\/flutter-live\/ &nbsp; https:\/\/flutter.io\/flutter-for-android\/ Flutter for Android [&hellip;]<\/p>\n",
      "protected": false
    },
    "author": 1,
    "featured_media": 0,
    "comment_status": "open",
    "ping_status": "open",
    "sticky": false,
    "template": "",
    "format": "standard",
    "meta": [
      
    ],
    "categories": [
      22,
      27,
      49,
      39
    ],
    "tags": [
      47
    ],
    "_links": {
      "self": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1463"
        }
      ],
      "collection": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts"
        }
      ],
      "about": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/types\/post"
        }
      ],
      "author": [
        {
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/users\/1"
        }
      ],
      "replies": [
        {
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/comments?post=1463"
        }
      ],
      "version-history": [
        {
          "count": 5,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1463\/revisions"
        }
      ],
      "predecessor-version": [
        {
          "id": 1613,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1463\/revisions\/1613"
        }
      ],
      "wp:attachment": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/media?parent=1463"
        }
      ],
      "wp:term": [
        {
          "taxonomy": "category",
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/categories?post=1463"
        },
        {
          "taxonomy": "post_tag",
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/tags?post=1463"
        }
      ],
      "curies": [
        {
          "name": "wp",
          "href": "https:\/\/api.w.org\/{rel}",
          "templated": true
        }
      ]
    }
  },
  {
    "id": 1452,
    "date": "2018-10-18T17:03:47",
    "date_gmt": "2018-10-18T14:03:47",
    "guid": {
      "rendered": "http:\/\/www.nickagas.com\/?p=1452"
    },
    "modified": "2018-10-18T17:10:40",
    "modified_gmt": "2018-10-18T14:10:40",
    "slug": "multi-os-engine-2",
    "status": "publish",
    "type": "post",
    "link": "https:\/\/www.nickagas.com\/multi-os-engine-2\/",
    "title": {
      "rendered": "Multi-OS Engine"
    },
    "content": {
      "rendered": "<h1 class=\"page__title\">Multi-OS Engine<\/h1>\n<p class=\"page__lead\">Create iOS Apps in Java<br \/>\n<span class=\"page__lead__small\">Port your existing Android App, or build a native Cross-Platform App from scratch.<\/span><\/p>\n<p><a class=\"btn btn--light-outline btn--large\" href=\"https:\/\/multi-os-engine.org\/start\/\"><i class=\"fa fa-download\"><\/i>\u00a0Get Started<\/a><\/p>\n<h1>Overview<\/h1>\n<p>The following image shows the typical development of mobile apps that target the two popular target platforms: Android and Apple iOS. It shows you how to create a simple Hello World app for Android and iOS devices that use shared common logic code that gets used by both app modules.<\/p>\n<p><img src=\"https:\/\/multi-os-engine.github.io\/doc\/_images\/Overview1.png\" alt=\"..\/..\/_images\/Overview1.png\" \/><\/p>\n<p>Now imagine if the above scenario changes to something like this:<\/p>\n<p><img src=\"https:\/\/multi-os-engine.github.io\/doc\/_images\/Overview2.png\" alt=\"..\/..\/_images\/Overview2.png\" \/><\/p>\n<p>Multi-OS Engine lets you develop native mobile applications for iOS and Android with only Java* expertise on Microsoft Windows and\/or Apple macOS development machines without compromising on the native look-and-feel or performance. You re-use as much common Java code as possible and add platform-specific UI code for each platform (two apps). And yes, you can develop iOS apps from Windows systems too!<\/p>\n<p>If you want to know \u201chow stuff works\u201d, continue reading below.<\/p>\n<p>&nbsp;<\/p>\n<p><a href=\"https:\/\/software.intel.com\/en-us\/articles\/videos-and-webinars-multi-os-engine\" target=\"_blank\" rel=\"noopener\">https:\/\/software.intel.com\/en-us\/articles\/videos-and-webinars-multi-os-engine<\/a><\/p>\n<p><a href=\"https:\/\/snow.dog\/blog\/moe-tutorial-1-create-simple-list-on-ios-and-android-apps-using-multi-os-engine\" target=\"_blank\" rel=\"noopener\">https:\/\/snow.dog\/blog\/moe-tutorial-1-create-simple-list-on-ios-and-android-apps-using-multi-os-engine<\/a><\/p>\n<p>https:\/\/medium.com\/@mateusz_bartos\/write-ios-apps-in-java-along-with-android-900d6013f83f<\/p>\n<p><a href=\"https:\/\/www.linkedin.com\/pulse\/we-built-our-ios-android-apps-java-kotlin-one-year-later-bartos\/\" target=\"_blank\" rel=\"noopener\">https:\/\/www.linkedin.com\/pulse\/we-built-our-ios-android-apps-java-kotlin-one-year-later-bartos\/<\/a><\/p>\n<p>https:\/\/www.youtube.com\/watch?v=M61HimidvK4&#038;feature=youtu.be<\/p>\n",
      "protected": false
    },
    "excerpt": {
      "rendered": "<p>Multi-OS Engine Create iOS Apps in Java Port your existing Android App, or build a native Cross-Platform App from scratch. \u00a0Get Started Overview The following image shows the typical development of mobile apps that target the two popular target platforms: Android and Apple iOS. It shows you how to create a simple Hello World app [&hellip;]<\/p>\n",
      "protected": false
    },
    "author": 1,
    "featured_media": 0,
    "comment_status": "open",
    "ping_status": "open",
    "sticky": false,
    "template": "",
    "format": "standard",
    "meta": [
      
    ],
    "categories": [
      22,
      27,
      39,
      21,
      35
    ],
    "tags": [
      
    ],
    "_links": {
      "self": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1452"
        }
      ],
      "collection": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts"
        }
      ],
      "about": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/types\/post"
        }
      ],
      "author": [
        {
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/users\/1"
        }
      ],
      "replies": [
        {
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/comments?post=1452"
        }
      ],
      "version-history": [
        {
          "count": 4,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1452\/revisions"
        }
      ],
      "predecessor-version": [
        {
          "id": 1456,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1452\/revisions\/1456"
        }
      ],
      "wp:attachment": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/media?parent=1452"
        }
      ],
      "wp:term": [
        {
          "taxonomy": "category",
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/categories?post=1452"
        },
        {
          "taxonomy": "post_tag",
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/tags?post=1452"
        }
      ],
      "curies": [
        {
          "name": "wp",
          "href": "https:\/\/api.w.org\/{rel}",
          "templated": true
        }
      ]
    }
  },
  {
    "id": 1448,
    "date": "2018-10-15T11:51:10",
    "date_gmt": "2018-10-15T08:51:10",
    "guid": {
      "rendered": "http:\/\/www.nickagas.com\/?p=1448"
    },
    "modified": "2018-10-15T11:51:10",
    "modified_gmt": "2018-10-15T08:51:10",
    "slug": "android-google-maps-java-lang-noclassdeffounderror-failed-resolution-of-lorg-apache-http-protocolversion",
    "status": "publish",
    "type": "post",
    "link": "https:\/\/www.nickagas.com\/android-google-maps-java-lang-noclassdeffounderror-failed-resolution-of-lorg-apache-http-protocolversion\/",
    "title": {
      "rendered": "Android Google maps java.lang.NoClassDefFoundError: Failed resolution of: Lorg\/apache\/http\/ProtocolVersion"
    },
    "content": {
      "rendered": "",
      "protected": false
    },
    "excerpt": {
      "rendered": "<p>App crashes on android 9 PIE java.lang.NoClassDefFoundError: Failed resolution of: Lorg\/apache\/http\/ProtocolVersion; at el.b(:com.google.android.gms.dynamite_mapsdynamite@12848063@12.8.48 (100408-196123505):3) at ek.a(:com.google.android.gms.dynamite_mapsdynamite@12848063@12.8.48 (100408-196123505):4) at em.a(:com.google.android.gms.dynamite_mapsdynamite@12848063@12.8.48 (100408-196123505):51) at com.google.maps.api.android.lib6.drd.ap.a(:com.google.android.gms.dynamite_mapsdynamite@12848063@12.8.48 (100408-196123505):11) at dw.a(:com.google.android.gms.dynamite_mapsdynamite@12848063@12.8.48 (100408-196123505):16) at dw.run(:com.google.android.gms.dynamite_mapsdynamite@12848063@12.8.48 (100408-196123505):61) Caused by: java.lang.ClassNotFoundException: Didn&#8217;t find class &#8220;org.apache.http.ProtocolVersion&#8221; on path: DexPathList[[zip file &#8220;\/system\/priv-app\/PrebuiltGmsCorePi\/app_chimera\/m\/MapsDynamite.apk&#8221;],nativeLibraryDirectories=[\/data\/user_de\/0\/com.google.android.gms\/app_chimera\/m\/00000036\/MapsDynamite.apk!\/lib\/armeabi-v7a, \/data\/user_de\/0\/com.google.android.gms\/app_chimera\/m\/00000036\/MapsDynamite.apk!\/lib\/armeabi, \/system\/lib]] at dalvik.system.BaseDexClassLoader.findClass(BaseDexClassLoader.java:126) at java.lang.ClassLoader.loadClass(ClassLoader.java:379) at ad.loadClass(:com.google.android.gms.dynamite_dynamiteloader@12848063@12.8.48 (100408-196123505):25) at java.lang.ClassLoader.loadClass(ClassLoader.java:312) at el.b(:com.google.android.gms.dynamite_mapsdynamite@12848063@12.8.48 (100408-196123505):3)\u00a0 [&hellip;]<\/p>\n",
      "protected": false
    },
    "author": 1,
    "featured_media": 0,
    "comment_status": "open",
    "ping_status": "open",
    "sticky": false,
    "template": "",
    "format": "standard",
    "meta": [
      
    ],
    "categories": [
      22,
      27
    ],
    "tags": [
      
    ],
    "_links": {
      "self": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1448"
        }
      ],
      "collection": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts"
        }
      ],
      "about": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/types\/post"
        }
      ],
      "author": [
        {
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/users\/1"
        }
      ],
      "replies": [
        {
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/comments?post=1448"
        }
      ],
      "version-history": [
        {
          "count": 1,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1448\/revisions"
        }
      ],
      "predecessor-version": [
        {
          "id": 1449,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1448\/revisions\/1449"
        }
      ],
      "wp:attachment": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/media?parent=1448"
        }
      ],
      "wp:term": [
        {
          "taxonomy": "category",
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/categories?post=1448"
        },
        {
          "taxonomy": "post_tag",
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/tags?post=1448"
        }
      ],
      "curies": [
        {
          "name": "wp",
          "href": "https:\/\/api.w.org\/{rel}",
          "templated": true
        }
      ]
    }
  },
  {
    "id": 1434,
    "date": "2018-10-03T11:51:22",
    "date_gmt": "2018-10-03T08:51:22",
    "guid": {
      "rendered": "http:\/\/www.nickagas.com\/?p=1434"
    },
    "modified": "2018-10-03T11:54:12",
    "modified_gmt": "2018-10-03T08:54:12",
    "slug": "android-tools-sample-data",
    "status": "publish",
    "type": "post",
    "link": "https:\/\/www.nickagas.com\/android-tools-sample-data\/",
    "title": {
      "rendered": "Android Layout Tools and Sample Data"
    },
    "content": {
      "rendered": "",
      "protected": false
    },
    "excerpt": {
      "rendered": "<p>I. Android tools attributes Error handling attributes Design-time view attributes (Don\u2019t miss this!) Resource shrinking attributes II. Sample Data (Best design helper!) &nbsp; I. Tools Attributes tools:shrinkMode tools:keep\u00a0and\u00a0tools:discard &nbsp; PODCAST EPISODES Yashish DuaFollow Mobile Developer &#8211; Loves to build products. Sep 24 Android Tools Attributes and Sample Data\u200a\u2014\u200aMust know\u00a0helpers! Every year at I\/O (Google\u2019s biggest [&hellip;]<\/p>\n",
      "protected": false
    },
    "author": 1,
    "featured_media": 0,
    "comment_status": "open",
    "ping_status": "open",
    "sticky": false,
    "template": "",
    "format": "standard",
    "meta": [
      
    ],
    "categories": [
      22,
      27
    ],
    "tags": [
      
    ],
    "_links": {
      "self": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1434"
        }
      ],
      "collection": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts"
        }
      ],
      "about": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/types\/post"
        }
      ],
      "author": [
        {
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/users\/1"
        }
      ],
      "replies": [
        {
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/comments?post=1434"
        }
      ],
      "version-history": [
        {
          "count": 2,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1434\/revisions"
        }
      ],
      "predecessor-version": [
        {
          "id": 1436,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1434\/revisions\/1436"
        }
      ],
      "wp:attachment": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/media?parent=1434"
        }
      ],
      "wp:term": [
        {
          "taxonomy": "category",
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/categories?post=1434"
        },
        {
          "taxonomy": "post_tag",
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/tags?post=1434"
        }
      ],
      "curies": [
        {
          "name": "wp",
          "href": "https:\/\/api.w.org\/{rel}",
          "templated": true
        }
      ]
    }
  },
  {
    "id": 1417,
    "date": "2018-09-30T16:34:24",
    "date_gmt": "2018-09-30T13:34:24",
    "guid": {
      "rendered": "http:\/\/www.nickagas.com\/?p=1417"
    },
    "modified": "2018-09-30T21:25:51",
    "modified_gmt": "2018-09-30T18:25:51",
    "slug": "materialize-css",
    "status": "publish",
    "type": "post",
    "link": "https:\/\/www.nickagas.com\/materialize-css\/",
    "title": {
      "rendered": "Materialize CSS"
    },
    "content": {
      "rendered": "<div class=\"col s12 m10 offset-m1\">\n<h1 class=\"header\">Materialize<\/h1>\n<\/div>\n<div class=\"col s12 m8 offset-m2\">\n<h4 class=\"light\">A modern responsive front-end framework based on Material Design<\/h4>\n<\/div>\n<p>https:\/\/materializecss.com\/getting-started.html<\/p>\n<p>Material Degidn Icons<\/p>\n<p>https:\/\/material.io\/tools\/icons\/?search=account&#038;icon=account_circle&#038;style=baseline<\/p>\n<h2 class=\"block\">What is jQuery?<\/h2>\n<p>jQuery is a fast, small, and feature-rich JavaScript library. It makes things like HTML document traversal and manipulation, event handling, animation, and Ajax much simpler with an easy-to-use API that works across a multitude of browsers. With a combination of versatility and extensibility, jQuery has changed the way that millions of people write JavaScript.<\/p>\n<p>https:\/\/jquery.com\/<\/p>\n<p>tutorial<\/p>\n<p><a href=\"https:\/\/www.nickagas.com\/materialize\/\" target=\"_blank\" rel=\"noopener\">https:\/\/www.nickagas.com\/materialize\/<\/a><\/p>\n<p><a href=\"https:\/\/github.com\/shadowmanpat\/materialize_css_example\" target=\"_blank\" rel=\"noopener\">https:\/\/github.com\/shadowmanpat\/materialize_css_example<\/a><\/p>\n<p><iframe title=\"Materialize Tutorial #1 - Introduction\" width=\"640\" height=\"360\" src=\"https:\/\/www.youtube.com\/embed\/videoseries?list=PL4cUxeGkcC9gGrbtvASEZSlFEYBnPkmff\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen><\/iframe><\/p>\n<p>&nbsp;<\/p>\n",
      "protected": false
    },
    "excerpt": {
      "rendered": "<p>Materialize A modern responsive front-end framework based on Material Design https:\/\/materializecss.com\/getting-started.html Material Degidn Icons https:\/\/material.io\/tools\/icons\/?search=account&#038;icon=account_circle&#038;style=baseline What is jQuery? jQuery is a fast, small, and feature-rich JavaScript library. It makes things like HTML document traversal and manipulation, event handling, animation, and Ajax much simpler with an easy-to-use API that works across a multitude of browsers. With [&hellip;]<\/p>\n",
      "protected": false
    },
    "author": 1,
    "featured_media": 0,
    "comment_status": "open",
    "ping_status": "open",
    "sticky": false,
    "template": "",
    "format": "standard",
    "meta": [
      
    ],
    "categories": [
      34
    ],
    "tags": [
      
    ],
    "_links": {
      "self": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1417"
        }
      ],
      "collection": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts"
        }
      ],
      "about": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/types\/post"
        }
      ],
      "author": [
        {
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/users\/1"
        }
      ],
      "replies": [
        {
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/comments?post=1417"
        }
      ],
      "version-history": [
        {
          "count": 5,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1417\/revisions"
        }
      ],
      "predecessor-version": [
        {
          "id": 1422,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1417\/revisions\/1422"
        }
      ],
      "wp:attachment": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/media?parent=1417"
        }
      ],
      "wp:term": [
        {
          "taxonomy": "category",
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/categories?post=1417"
        },
        {
          "taxonomy": "post_tag",
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/tags?post=1417"
        }
      ],
      "curies": [
        {
          "name": "wp",
          "href": "https:\/\/api.w.org\/{rel}",
          "templated": true
        }
      ]
    }
  },
  {
    "id": 1414,
    "date": "2018-09-24T11:04:42",
    "date_gmt": "2018-09-24T08:04:42",
    "guid": {
      "rendered": "http:\/\/www.nickagas.com\/?p=1414"
    },
    "modified": "2018-09-24T11:04:42",
    "modified_gmt": "2018-09-24T08:04:42",
    "slug": "androidx-refactoring",
    "status": "publish",
    "type": "post",
    "link": "https:\/\/www.nickagas.com\/androidx-refactoring\/",
    "title": {
      "rendered": "AndroidX refactoring"
    },
    "content": {
      "rendered": "",
      "protected": false
    },
    "excerpt": {
      "rendered": "<p>AndroidX Overview We are rolling out a new package structure to make it clearer which packages are bundled with the Android operating system, and which are packaged with your app&#8217;s APK. Going forward, the\u00a0android.*\u00a0package hierarchy will be reserved for Android packages that ship with the operating system; other packages will be issued in the new\u00a0androidx.*package [&hellip;]<\/p>\n",
      "protected": false
    },
    "author": 1,
    "featured_media": 0,
    "comment_status": "open",
    "ping_status": "open",
    "sticky": false,
    "template": "",
    "format": "standard",
    "meta": [
      
    ],
    "categories": [
      22,
      27
    ],
    "tags": [
      
    ],
    "_links": {
      "self": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1414"
        }
      ],
      "collection": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts"
        }
      ],
      "about": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/types\/post"
        }
      ],
      "author": [
        {
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/users\/1"
        }
      ],
      "replies": [
        {
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/comments?post=1414"
        }
      ],
      "version-history": [
        {
          "count": 1,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1414\/revisions"
        }
      ],
      "predecessor-version": [
        {
          "id": 1415,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/posts\/1414\/revisions\/1415"
        }
      ],
      "wp:attachment": [
        {
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/media?parent=1414"
        }
      ],
      "wp:term": [
        {
          "taxonomy": "category",
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/categories?post=1414"
        },
        {
          "taxonomy": "post_tag",
          "embeddable": true,
          "href": "https:\/\/www.nickagas.com\/wp-json\/wp\/v2\/tags?post=1414"
        }
      ],
      "curies": [
        {
          "name": "wp",
          "href": "https:\/\/api.w.org\/{rel}",
          "templated": true
        }
      ]
    }
  }
]